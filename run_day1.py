from bin.file_io import get_input_lines
from bin.calculations import get_calories_per_elf, get_sum_top3_most_calories


lines = get_input_lines("input_day1.txt")
calories_list = get_calories_per_elf(lines)
most_calories = max(calories_list)
print(f"The elf with the most calories is carrying {most_calories} calories")

top3_calories = get_sum_top3_most_calories(calories_list)
print(f"The three elves with the most calories are carrying {top3_calories} calories")
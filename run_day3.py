from bin.file_io import get_input_lines

PRIORITIES = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8, 'i': 9, 'j': 10, 'k': 11, 'l': 12, 'm': 13, 'n': 14, 'o': 15, 'p': 16, 'q': 17, 'r': 18, 's': 19, 't': 20, 'u': 21, 'v': 22, 'w': 23, 'x': 24, 'y': 25, 'z': 26, 'A': 27, 
'B': 28, 'C': 29, 'D': 30, 'E': 31, 'F': 32, 'G': 33, 'H': 34, 'I': 35, 'J': 36, 'K': 37, 'L': 38, 'M': 39, 'N': 40, 'O': 41, 'P': 42, 'Q': 43, 'R': 44, 'S': 45, 'T': 46, 'U': 47, 'V': 48, 'W': 49, 'X': 50, 'Y': 51, 'Z': 52}

lines = get_input_lines("input_day3.txt")

def get_shared_item(comp1: str, comp2: str) -> set:
    set1 = set(comp1)
    set2 = set(comp2)
    intersect = set1.intersection(set2)
    item = ''.join(intersect)
    return item

def get_item_priority(item: str) -> int:
    return PRIORITIES[item]

def get_compartments(rucksack: str) -> tuple[str]:
    comp_size = int(len(rucksack)/2)
    comp1 = rucksack[:comp_size]
    comp2 = rucksack[comp_size:]
    return comp1,comp2

total = 0
for line in lines:
    comp1,comp2 = get_compartments(line)
    item = str(get_shared_item(comp1, comp2))
    # print(item)
    priority = get_item_priority(item)
    total += priority

print(f"Total priority of all misplaced items: {total}")
import pytest

from bin import calculations as calc

@pytest.fixture(scope='function')
def stub_calorie_input():
    yield ['5','10','','3','2','','7','3']

@pytest.fixture(scope='function')
def stub_calories_list():
    yield [10, 11, 9, 24, 72, 55]

def test_get_calories_per_elf(stub_calorie_input):
    expected = [15, 5, 10]
    calorie_list = calc.get_calories_per_elf(stub_calorie_input)
    assert calorie_list == expected

def test_get_sum_top_3_most_calories(stub_calories_list):
    expected = 151
    top3_sum = calc.get_sum_top3_most_calories(stub_calories_list)
    assert top3_sum == expected

def test_get_score_round_opp_A_outcome_X():
    expected = 3
    score = calc.get_score_round('A', 'X')
    assert score == expected

def test_get_score_round_opp_A_outcome_Y():
    expected = 4
    score = calc.get_score_round('A', 'Y')
    assert score == expected

def test_get_score_round_opp_A_outcome_Z():
    expected = 8
    score = calc.get_score_round('A', 'Z')
    assert score == expected

def test_read_input_rps_A_X():
    expected_opp = 'A'
    expected_player = 'X'
    opp,player = calc.read_input_rps('A X\n')
    assert expected_opp == opp
    assert expected_player == player


from bin.file_io import get_input_lines
from bin import calculations as calc

# A = Rock = 1
# B = Paper = 2
# C = Scissors = 3
# X = lose
# Y = draw
# Z = win
# Win = 6
# Draw = 3
# Loss = 0

lines = get_input_lines("input_day2.txt")
score = 0
for line in lines:
    opp, outcome = calc.read_input_rps(line)
    score += calc.get_score_round(opp, outcome)

print(f"Score: {score}")

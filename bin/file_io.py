def get_input_lines(filepath: str) -> list:
    with open(filepath) as file:
        return file.readlines()
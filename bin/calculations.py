def get_calories_per_elf(lines: list[str]) -> list[int]:
    calorie_list = []
    current_calories = 0
    for line in lines:
        if line.strip() == "":
            calorie_list.append(current_calories)
            current_calories = 0
        else:
            current_calories += int(line)
    calorie_list.append(current_calories)
    return calorie_list

def get_sum_top3_most_calories(calories_list: list[int]) -> int:
    total = 0
    for _ in range(3):
        total += max(calories_list)
        calories_list.remove(max(calories_list))
    return total

def get_score_round(opp: str, outcome: str) -> int:
    # Rock = 1
    # Paper = 2
    # Scissors = 3
    
    if opp == 'A': # Rock
        match outcome:
            case 'X': # Lose - Scissors
                return 0 + 3
            case 'Y': # Draw - Rock
                return 3 + 1
            case 'Z': # Win - Paper
                return 6 + 2
    if opp == 'B': # Paper
        match outcome:
            case 'X': # Lose - Rock
                return 0 + 1
            case 'Y': # Draw - Paper
                return 3 + 2
            case 'Z': # Win - Scissors
                return 6 + 3
    if opp == 'C': # Scissors
        match outcome:
            case 'X': # Lose - Paper
                return 0 + 2
            case 'Y': # Draw - Scissors
                return 3 + 3
            case 'Z': # Win - Rock
                return 6 + 1

def read_input_rps(round: str) -> (str,str):
    return round[0],round[2]
